#include <vector>
#include <ArduinoJson.h>
#include <Reflector.h>

#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <FS.h>


class SsidSettings{
public:
    String ssid;
    String password;

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (ssid, "ssid", String("ssid"), "Name")
                (password, "password", String("password"), "Password");
    }
};

class Settings {
    const char *  fileName = "/config.json";
public:
    SsidSettings ssidSettings;
    std::vector<String> stringList;

    template<class Reflector>
    void reflect(Reflector& reflector) {
        reflector
                (ssidSettings, "ssidSettings", SsidSettings(), "SSID Settings")
                (stringList, "stringList", {"a", "b", "c"}, "Some String List");
    }

    void load () {
        File file = SPIFFS.open(fileName, "r");
        DynamicJsonBuffer jsonBuffer;
        JsonObject& root = jsonBuffer.parseObject(file);
        JsonDeserializer<JsonObject> deserializer(root);
        reflect(deserializer);
        file.close();
    }

    void save () {
      File file = SPIFFS.open(fileName, "w");
      DynamicJsonBuffer jsonBuffer;
      JsonObject& root = jsonBuffer.createObject();
      JsonSerializer<JsonObject> serializer(root);
      reflect(serializer);
      root.printTo(file);
      file.close();
    }
};

Settings settings;
ESP8266WebServer server(80);

bool connectToSsid() {
    WiFi.mode(WIFI_STA);
    WiFi.begin(settings.ssidSettings.ssid.c_str(), settings.ssidSettings.password.c_str());

    int counter = 10;
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.println(".");
        counter--;
        if(counter == 0) {
            WiFi.disconnect();
            return false;
        }
    }
    return true;
}

void getSettingsData() {
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonMetaSerializer<JsonObject> serializer(root);
    settings.reflect(serializer);
    String response;
    root.printTo(response);
    server.send(200, "application/json", response);
}

void postSettingsData() {
    String plain = server.arg("plain");
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parse(plain);
    JsonMetaDeserializer<JsonObject> deserializer(root);
    settings.reflect(deserializer);
    settings.save();
    getSettingsData();
}

void setup() {
    SPIFFS.begin();
    Serial.begin(115200);
    settings.load();

    Serial.print("Connecting ");
    if(connectToSsid()) {
        Serial.println("Connected IP: " + WiFi.localIP().toString());
    } else {
        Serial.println("Connection failed");
    }

    server.on("/settings_data", HTTP_POST, postSettingsData);
    server.on("/settings_data", HTTP_GET, getSettingsData);

    server.serveStatic("/", SPIFFS, "/index.html");
    server.serveStatic("/index.js", SPIFFS, "/index.js");
    server.serveStatic("/config.json", SPIFFS, "/config.json");
    server.begin();
}

void loop() {
    server.handleClient();
}
