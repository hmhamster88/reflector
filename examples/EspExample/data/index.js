function getJson(url) {
  return new Promise(function(resolve, reject) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function()
    {
      if(xmlHttp.readyState == 4)
      {
        if(xmlHttp.status == 200) {
          resolve(JSON.parse(xmlHttp.responseText));
        } else {
          reject(xmlHttp);
        }
      }
    }
    xmlHttp.open('get', url);
    xmlHttp.send();
  });
}

function postJson(url, data) {
  return new Promise(function(resolve, reject) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function()
    {
      if(xmlHttp.readyState == 4)
      {
        if(xmlHttp.status == 200) {
          resolve(JSON.parse(xmlHttp.responseText));
        } else {
          reject(xmlHttp);
        }
      }
    }
    xmlHttp.open('post', url);
    xmlHttp.send(JSON.stringify(data));
  });
}

var valueGetters = {
  number () {
    return Number.parseInt(this.value);
  },
  string () {
    return this.value;
  },
  boolean() {
    return this.checked === 'true';
  }
}

const inputTypes = {
  boolean: 'checkbox'
}

function getInputType(value) {
  const typeName = typeof value;
  return inputTypes[typeName] || typeName;
}


function updateFields(fields, settings) {
  while (fields.firstChild) {
    fields.removeChild(fields.firstChild);
  }
  var props = Array.isArray(settings) ? settings : Object.getOwnPropertyNames(settings);
  for(var propName of props) {
    var propertyMeta = Array.isArray(settings) ? propName : settings[propName];
    var span = document.createElement('span');
    span.innerHTML = propertyMeta.displayName;
    fields.appendChild(span);
    fields.appendChild(document.createElement('br'));
    if (typeof propertyMeta.value === "object") {
        var div = document.createElement('div');
        div.setAttribute('style', 'margin-left: 20px');
        updateFields(div, propertyMeta.value);
        fields.appendChild(div);
    } else {
        var input = document.createElement('input');
        input.setAttribute('type', getInputType(propertyMeta.value));
        input.setAttribute('value', propertyMeta.value);
        input.setAttribute('name', propName);
        input.propertyMeta = propertyMeta;
        input.onchange = function () {
            this.propertyMeta.value = this.getVal();
        }
        input.getVal = valueGetters[typeof propertyMeta.value];
        fields.appendChild(input)
    }
    fields.appendChild(document.createElement('br'))
  }
}

window.onload = () => {
  var fields = document.getElementById('fields');
  var saveButton = document.getElementById('save-button');
  var restartButton = document.getElementById('restart-button');
  var settings = {};

  getJson('/settings_data')
      .then(data => {
        settings = data;
        updateFields(fields, settings);
      })
      .catch(data => console.log(data));


  saveButton.onclick = () => {
    postJson('/settings_data', settings)
        .then(data => console.log(data))
        .catch(data => console.log(data));
  };

  restartButton.onclick = () => {
      postJson('/restart')
          .then(data => console.log(data))
          .catch(data => console.log(data));
  };
}
