#ifndef JSON_META_REFLECTOR_H
#define JSON_META_REFLECTOR_H

template<typename NodeType>
class JsonMetaSerializer
{
    NodeType& node;

    inline JsonObject& createNestedObject(const char* name, JsonObject& node) {
        return node.createNestedObject(name);
    }

    inline JsonObject& createNestedObject(const char* name, JsonArray& node) {
        return node.createNestedObject();
    }

public:
    JsonMetaSerializer(NodeType& node) : node(node) {}

    template<class T>
    inline JsonMetaSerializer& operator()( T & val, const char* name, const T& def = T(), const char* displayName = "") {
        FieldInfo<T> fi = {val, def, name, displayName};
        JsonObject& valueNode = createNestedObject(name, node);
        JsonMetaSerializer<JsonObject> serializer(valueNode);
        ReflectCaller::reflect(serializer, fi);
        return *this;
    }

    template<typename T>
    inline void writeMeta(FieldInfo<T>& fieldInfo) {
        node["displayName"] = fieldInfo.displayName;
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo) {
      writeMeta(fieldInfo);
      node["value"] = fieldInfo.value;
    }

    template<typename T>
    inline void processList(FieldInfo<T>& fieldInfo) {
      writeMeta(fieldInfo);
      JsonArray& jsonArray = node.createNestedArray("value");
      ListWrapper<T> wrapper(fieldInfo.value);
      for(typename ListWrapper<T>::ElementType element: wrapper) {
          JsonMetaSerializer<JsonArray> serializer(jsonArray);
          serializer(element, "", element);
      }
    }

    template<typename T>
    inline void processObject(FieldInfo<T>& fieldInfo) {
        writeMeta(fieldInfo);
        JsonObject& valueNode = node.createNestedObject("value");
        JsonMetaSerializer<JsonObject> serializer(valueNode);
        fieldInfo.value.reflect(serializer);
    }
};


template<typename NodeType>
class JsonMetaDeserializer {
    NodeType &node;

public:
    JsonMetaDeserializer(NodeType &node) : node(node) {}

    template<class T>
    inline JsonMetaDeserializer& operator()( T & val, const char* name, const T& def = T(), const char* displayName = "") {
        FieldInfo<T> fi = {val, def, name, displayName};
        ReflectCaller::reflect(*this, fi);
        return *this;
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonObject& object) {
        BasicJsonSetter<T>::set(fieldInfo, object[fieldInfo.name]["value"]);
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonArray& array) {
        BasicJsonSetter<T>::set(fieldInfo, array[fieldInfo.index]["value"]);
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo) {
      processBasic(fieldInfo, node);
    }

    template<typename T>
    inline void processList(FieldInfo<T>& fieldInfo) {
        JsonArray& array = node[fieldInfo.name]["value"];
        if(array.success()) {
            int index = 0;
            ListWrapper<T> wrapper(fieldInfo.value);
            wrapper.clear();
            JsonMetaDeserializer<JsonArray> deserializer(array);
            for (JsonObject &element: array) {
                typename ListWrapper<T>::ElementType mock;
                FieldInfo<typename ListWrapper<T>::ElementType> fi = {mock, mock, "", "", index};
                ReflectCaller::reflect(deserializer, fi);
                index++;
                wrapper.add(fi.value);
            }
        } else {
            fieldInfo.value = fieldInfo.defaultValue;
        }
    }

    template<typename T>
    inline void processObject(FieldInfo<T>& fieldInfo) {
        JsonObject& subNode = node[fieldInfo.name]["value"];
        if (subNode.success()) {
            JsonMetaDeserializer<JsonObject> deserializer(subNode);
            fieldInfo.value.reflect(deserializer);
        } else {
            fieldInfo.value = fieldInfo.defaultValue;
        }
    }
};

#endif

