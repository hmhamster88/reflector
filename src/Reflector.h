#ifndef REFLECTOR_H
#define REFLECTRO_H

template<typename T>
class ListWrapper {
public:
    enum {
        isList = false
    };

};

#ifndef REFLECTOR_USE_VECTOR
	#define REFLECTOR_USE_VECTOR 1
#endif

#if REFLECTOR_USE_VECTOR == 1

template<typename T>
class ListWrapper<std::vector<T>> {
  std::vector<T>& vector;
public:
    typedef T ElementType;
    
    enum {
        isList = true
    };

    ListWrapper(std::vector<T>& vector) : vector(vector) {}

    inline void add(const T &t) {
        vector.push_back(t);
    }

    inline T &get(int index) {
        return vector[index];
    }

    inline void clear() {
        vector.clear();
    }

    inline typename std::vector<T>::iterator begin() {
      return vector.begin();
    }

    inline typename std::vector<T>::iterator end() {
      return vector.end();
    }
};

#endif

template<typename T>
struct is_class {
    template<typename C>
    static char func(char C::*p);

    template<typename C>
    static int func(...);

    enum {
        value = sizeof(is_class<T>::template func<T>(0)) == 1
    };
};

template<>
struct is_class<String> {
    enum {
        value = false
    };
};

class FieldType {
public:
    enum {
        Simple, Object, List
    };
};

template<typename T>
class TypeGetter {
public:
    enum {
        value = is_class<T>::value ? (ListWrapper<T>::isList ? FieldType::List : FieldType::Object) : FieldType::Simple
    };
};

template <typename T>
struct FieldInfo {
    T& value;
    const T& defaultValue;
    const char* name;
    const char* displayName;
    int index;
};

template<int>
class TypeSelector {
};

template<>
class TypeSelector<FieldType::Simple> {
public:
    template <typename Reflector, typename T>
    inline static void reflect(Reflector& reflector, FieldInfo<T>& fieldInfo) {
      reflector.processBasic(fieldInfo);
    }
};

template<>
class TypeSelector<FieldType::Object> {
public:
    template <typename Reflector, typename T>
    inline static void reflect(Reflector& reflector, FieldInfo<T>& fieldInfo) {
        reflector.processObject(fieldInfo);
    }
};

template<>
class TypeSelector<FieldType::List> {
public:
    template <typename Reflector, typename T>
    inline static void reflect(Reflector& reflector, FieldInfo<T>& fieldInfo) {
      reflector.processList(fieldInfo);
    }
};

class ReflectCaller {
  public:
  template<typename Reflector, typename T>
  static inline void reflect(Reflector& reflector, FieldInfo<T>& fieldInfo) {
    TypeSelector<TypeGetter<T>::value>::reflect(reflector, fieldInfo);
  }
};

#ifdef ARDUINOJSON_EMBEDDED_MODE
#include "./JsonReflector.h"
#include "./JsonMetaReflector.h"
#endif


#endif

