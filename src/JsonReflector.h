#ifndef JSON_H
#define JSON_H

template <typename NodeType>
class JsonSerializer
{
  NodeType& node;

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonObject& object) {
      object[fieldInfo.name] = fieldInfo.value;
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonArray& array) {
      array.add(fieldInfo.value);
    }

    inline JsonObject& createNestedObject(const char* name, JsonObject& node) {
      return node.createNestedObject(name);
    }

    inline JsonObject& createNestedObject(const char* name, JsonArray& node) {
      return node.createNestedObject();
    }

    inline JsonArray& createNestedArray(const char* name, JsonObject& node) {
      return node.createNestedArray(name);
    }

    inline JsonArray& createNestedArray(const char* name, JsonArray& node) {
      return node.createNestedArray();
    }

  
  public:
    JsonSerializer(NodeType& node):
      node(node)
    {
      
    }
    
    template<class T>
    inline JsonSerializer& operator()( T & val, const char* name, const T& def = T(), const char* displayName = "") {
        FieldInfo<T> fi = {val, def, name, displayName};
        ReflectCaller::reflect(*this, fi);
        return *this;
    }
    
    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo) {
      processBasic(fieldInfo, node);
    }

    template<typename T>
    inline void processList(FieldInfo<T>& fieldInfo) {
      JsonArray& newNode = createNestedArray(fieldInfo.name, node);
      JsonSerializer<JsonArray> serializer(newNode);
      ListWrapper<T> wrapper(fieldInfo.value);
      for(typename ListWrapper<T>::ElementType element: wrapper) {
         serializer(element, "", element);
      }
    }

    template<typename T>
    inline void processObject(FieldInfo<T>& fieldInfo) {
      JsonObject& newNode = createNestedObject(fieldInfo.name, node);
      JsonSerializer<JsonObject> serializer(newNode); 
      fieldInfo.value.reflect(serializer);
    }
};

template<typename T>
struct BasicJsonSetter {
    inline static void set(FieldInfo<T>& fieldInfo, const JsonVariant& var) {
        fieldInfo.value = var | fieldInfo.defaultValue;
    }
};

template<>
struct BasicJsonSetter<String> {
    inline static void set(FieldInfo<String>& fieldInfo, const JsonVariant& var) {
        if (var.is<char*>()) {
            fieldInfo.value = var.as<String>();
        } else {
            fieldInfo.value = fieldInfo.defaultValue;
        }
    }
};

template <typename NodeType>
class JsonDeserializer
{
  NodeType& node;


    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonObject& object) {
        BasicJsonSetter<T>::set(fieldInfo, object[fieldInfo.name]);
    }

    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo, JsonArray& array) {
        BasicJsonSetter<T>::set(fieldInfo, array[fieldInfo.index]);
    }
    
    template<typename T>
    inline JsonObject& getNestedObject(FieldInfo<T>& fieldInfo, JsonObject& node) {
      return node[fieldInfo.name];
    }

    template<typename T>
    inline JsonObject& getNestedObject(FieldInfo<T>& fieldInfo, JsonArray& node) {
      return node[fieldInfo.index];
    }

    template<typename T>
    inline JsonArray& getNestedArray(FieldInfo<T>& fieldInfo, JsonObject& node) {
      return node[fieldInfo.name];
    }

    template<typename T>
    inline JsonArray& getNestedArray(FieldInfo<T>& fieldInfo, JsonArray& node) {
      return node[fieldInfo.index];
    }

  
  public:
    JsonDeserializer(NodeType& node):
      node(node)
    {
      
    }
    
    template<class T>
    inline JsonDeserializer& operator()( T & val, const char* name, const T& def = T(), const char* displayName = "") {
        FieldInfo<T> fi = {val, def, name, displayName};
        ReflectCaller::reflect(*this, fi);
        return *this;
    }
    
    template<typename T>
    inline void processBasic(FieldInfo<T>& fieldInfo) {
      processBasic(fieldInfo, node);
    }

    template<typename T>
    inline void processList(FieldInfo<T>& fieldInfo) {
      JsonArray& array = getNestedArray(fieldInfo, node);
      if(array.success()) {
        JsonDeserializer<JsonArray> deserializer(array); 
        int index = 0;
        ListWrapper<T> wrapper(fieldInfo.value);
        wrapper.clear();
        for(JsonVariant& element: array) {
          typename ListWrapper<T>::ElementType mock;
          FieldInfo<typename ListWrapper<T>::ElementType> fi = {mock, mock, "", "", index};
          ReflectCaller::reflect(deserializer, fi);
          index++;
          wrapper.add(fi.value);
        }
      } else {
        fieldInfo.value = fieldInfo.defaultValue;
      }
    }

    template<typename T>
    inline void processObject(FieldInfo<T>& fieldInfo) {
      JsonObject& subNode = getNestedObject(fieldInfo, node);
      if (subNode.success()) {
        JsonDeserializer<JsonObject> deserializer(subNode);
        fieldInfo.value.reflect(deserializer);
      } else {
        fieldInfo.value = fieldInfo.defaultValue;
      }
    }
};

#endif

